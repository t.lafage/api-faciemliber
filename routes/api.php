<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth
Route::post('users/auth', 'UserController@auth');
Route::post('users/register', 'UserController@register');

// User
Route::resource('users', 'UserController')->only(['store', 'index', 'show']);

Route::get('users/{user_id}/friends', 'UserController@indexFriends');

Route::get('users/pseudo/{pseudo}', 'UserController@findWithPseudo');
Route::get('users/post/{id}', 'UserController@findLikesForAPost');
Route::post('users/search', 'UserController@findWithFirstsLetters');

Route::post('users/add/friend', 'UserController@storeAFriendship');

// Post
Route::resource('posts', 'PostController')->only(['store', 'index', 'show']);

Route::get('posts/{pseudo}', 'PostController@findAllForAUser'); // ?? - ??
Route::get('posts/{user_id}/news', 'PostController@indexFriendPostForAUser');
Route::get('posts/{user_id}/wall', 'PostController@AllPersonalPostsForAUser');

Route::post('posts/add/like', 'PostController@storeAlike');

// Comment
Route::resource('comments', 'CommentController')->only(['store', 'index', 'show']);

Route::get('comments/post/{id}', 'CommentController@findAllForAPost');
