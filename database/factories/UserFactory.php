<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'pseudo' => $faker->userName(),
        'password' => $faker->sentence(4),
        'email' => $faker->unique()->safeEmail,
        'anniversary' => $faker->dateTimeBetween('-36 years', '-15 years'),
        'image' => random_int( 1, 25) . '.jpeg'
    ];
});
