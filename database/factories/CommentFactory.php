<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->text(100),
        'date_creation' => $faker->dateTimeBetween('-3 days', '0 days'),
        'author_id' => App\User::All()->random()->id,
        'post_id' => App\Post::All()->random()->id
    ];
});
