<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'content' => $faker->text(250),
        'date_creation' => $faker->dateTimeBetween('-6 days', '-3 days'),
        'author_id' => App\User::All()->random()->id
    ];
});
