<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Friendship;
use App\Like;
use App\Post;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return response()->json($users->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function indexFriends(int $user_id)
    {
        $users = Friendship::with('friend')
            ->where('user_id', '=', $user_id)
            ->get();

        return response()->json($users->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }


    public function findWithPseudo(string $pseudo)
    {
        // dd($pseudo);
        $users = User::where('pseudo', '=', $pseudo)->get();

        // dd($users[0]);

        return response()->json($users[0]->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function findWithFirstsLetters(Request $request)
    {
        $users = User::where('name', 'LIKE', $request->get('filter') . '%')->get();

        return response()->json($users->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function findLikesForAPost(int $post_id)
    {
        $likes = Post::find($post_id)->likes;

        $users = [];

        foreach ($likes as $like) {
            // dd($like['post_id']);
            $users[] = user::find($like['user_id']);
        }

        // $users = Like::with('user')
        //     ->where('post_id', '=', $post_id)
        //     ->get();

        return response()->json($users)
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function storeAFriendship(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id' => 'required',
            'friend_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors())
                ->header('Access-Control-Allow-Origin', '*');
        }

        $friendship = Friendship::create($request->all());

        return response()->json($friendship->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (is_null($user)) {
            return response()->json('User not found')
                ->header('Access-Control-Allow-Origin', '*');
        }

        return response()->json($user->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }

    public function auth(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors())
                ->header('Access-Control-Allow-Origin', '*');
        }

        // $inte = Hash::check($input['password']);

        // return response()->json(['password' => 'Login failed.'])
        //     ->header('Access-Control-Allow-Origin', '*');

        $user = User::where('email', $input['email'])->first();

        if ($user && Hash::check($request->password, $user->password)) {
            return response()->json($user->toArray())
                ->header('Access-Control-Allow-Origin', '*');
        }

        return response()->json(['error' => 'Login failed.'])
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function register(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'pseudo' => 'required',
            'anniversary' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors())
                ->header('Access-Control-Allow-Origin', '*');
        }

        $input['password'] = bcrypt($input['password']);

        $user = User::create($input);

        return response()->json($user->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }
}
