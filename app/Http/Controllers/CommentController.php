<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::with('author')->get();

        return response()->json($comments->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function findAllForAPost($id)
    {
        // $comments = Post::find($id)->comments;

        $comments = Comment::with('author')
            ->where('post_id', '=', $id)
            ->get();

        return response()->json($comments->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add([
            'date_creation' => date("Y-m-d H:i:s")
        ]);

        $input = $request->all();

        $validator = Validator::make($input, [
            'content' => 'required',
            'date_creation' => 'required',
            'author_id' => 'required',
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors())
                ->header('Access-Control-Allow-Origin', '*');
        }

        $post = Comment::create($request->all());

        return response()->json($post->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
