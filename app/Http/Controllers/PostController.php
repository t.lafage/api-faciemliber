<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Post;
use App\User;
use App\Like;
use App\Friendship;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('author')
            ->withCount('comments')
            ->withCount('likes')
            ->orderBy('date_creation', 'desc')
            ->get();

        return response()->json($posts->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function indexFriendPostForAUser($user_id)
    {
        $users = Friendship::where('user_id', '=', $user_id)->get('friend_id')->toArray();

        $posts = [];

        foreach ($users as $user) {
                // dd($user['friend_id']);

                $posts[] = Post::with('author')
                    ->where('author_id', '=', $user['friend_id'])
                    ->withCount('comments')
                    ->withCount('likes')
                    ->orderBy('date_creation', 'desc')
                    ->get()
                    ->toArray();
        };


        return response()->json($posts)
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function AllPersonalPostsForAUser(int $user_id)
    {

        // $author_id = User::find($pseudo);

        // dd($user_id);

        $posts = Post::with('author')
            ->where('author_id', '=', $user_id)
            ->withCount('comments')
            ->withCount('likes')
            ->orderBy('date_creation', 'desc')
            ->get();

        return response()->json($posts->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    // public function AllFriendPostsForAUser( $pseudo )
    // {
    //     $posts = Post::with('author')
    //         ->withCount('comments')
    //         ->orderBy('date_creation', 'desc')
    //         ->get();

    //     return response()->json($posts->toArray())
    //         ->header('Access-Control-Allow-Origin', '*');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->request->add([
            'date_creation' => date("Y-m-d H:i:s")
        ]);

        $input = $request->all();

        $validator = Validator::make($input, [
            'content' => 'required',
            'date_creation' => 'required',
            'author_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors())
                ->header('Access-Control-Allow-Origin', '*');
        }

        $post = Post::create($request->all());

        return response()->json($post->toArray())
            ->header('Access-Control-Allow-Origin', '*');
        // ->header('Access-Control-Allow-Methods', 'GET, POST');
        // ->header('Content-Type', 'application/x-www-form-urlencoded');
    }

    public function storeALike(Request $request)
    {

        $input = $request->all();

        $validator = Validator::make($input, [
            'user_id' => 'required',
            'post_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors())
                ->header('Access-Control-Allow-Origin', '*');
        }

        $like = Like::create($request->all());

        return response()->json($like->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        // if (is_null($post->author)) {
        //     return response()->json('User not found')
        //         ->header('Access-Control-Allow-Origin', '*');
        // }

        $post_auth = $post::with('author')->get();

        return response()->json($post_auth->toArray())
            ->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
