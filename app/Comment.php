<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Comment extends Model
{
    use Notifiable;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content', 'date_creation', 'author_id', 'post_id',
    ];

    function author() {
        return $this->belongsTo( User::class );
    }

    function post() {
        return $this->belongsTo( Post::class );
    }
}
