<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'pseudo', 'anniversary', 'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    // function posts() {
    //     return $this->hasMany( Post::class );
    // }

    function comments() {
        return $this->hasMany( Comment::class );
    }

    //-----//

    function friends() {
        return $this->belongsToMany( User::class );
    }

    public function friendships()
    {
        return $this->hasMany(Friendship::class);
    }

    //-----//

    function posts() {
        return $this->belongsToMany( Post::class );
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }
}
