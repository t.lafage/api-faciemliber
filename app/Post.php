<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
    use Notifiable;

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content', 'date_creation', 'author_id',
    ];

    function comments() {
        return $this->hasMany( Comment::class );
    }

    function author() {
        return $this->belongsTo( User::class );
    }

    //-------//

    function users() {
        return $this->belongsToMany( User::class );
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }
}
